# Desafio Push Ecommerce - Vaga Frontend
Implementar o design compartilhado pelo link abaixo, utilizando HTML, JS e CSS. 
Sinta-se a vontade para utilizar frameworks front-end (tailwind, Bootstrap, foundation, etc..) para acelerar seu desenvolvimento.

Visualização:
https://www.figma.com/file/yyBaTn91IiHbLr0sCaP307/Teste-Developers?node-id=65%3A0

Download para edição:
https://pushecommerce.com.br/wp-content/uploads/2022/10/Teste-Developers.zip


## Requisitos
- Ter conhecimento de Git
- Conhecimento de HTML, CSS e JS
- Fazer um fork deste repositório para sua conta do Gitlab.

## O que vamos avaliar
- Capacidade de apredizado
- Capacidade de criar interfaces com alta fidelidade a partir de layouts/protótipos.
- Semântica do seu código HTML
- Estruturação do seu CSS de maneira reaproveitável e utilização de especificações mais recentes (flexbox, grid layout)
- Boa lógica de programação e utilização de boas práticas no seu código javascript.

## Bonus Points:
- Utilização de features da especificação ES6
- Automação com Npm scripts
- Aplicação de Rich Snippet e dados estruturados
- Utilizar CMS como wordpress e construir um tema

## Submissão
Após finalizar a implementação enviar o link do repositório para: 
- claudio@pushecommerce.com.br
- Seu Nome:
- Email:
- Link do Repositório:

# Boa sorte ;)
